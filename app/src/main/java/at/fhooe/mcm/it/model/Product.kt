package at.fhooe.mcm.it.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class Product(@SerializedName("_id")
                   val id: String,
                   val title: String,
                   val description: String,
                   val price: Double,
                   val imageUrl: String,
                   val category: String,
                   val createdAt: Date,
                   @SerializedName("__v")
                   val version: Int,
                   val updatedAt: Date) : Serializable