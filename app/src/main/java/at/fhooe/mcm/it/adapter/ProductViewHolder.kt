package at.fhooe.mcm.it.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import at.fhooe.mcm.it.model.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*

class ProductViewHolder(private val view: View, private val listener: ProductClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {

    private var product: Product? = null

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        listener.onClickProduct(adapterPosition)
    }

    fun bindProduct(product: Product) {
        this.product = product

        Picasso.get().load(product.imageUrl).into(view.productImage)
        view.productTitle.text = product.title
        view.productDescription.text = product.description
    }

}