package at.fhooe.mcm.it.adapter

interface CategoryClickListener {
    fun onClickCategory(position: Int)
}