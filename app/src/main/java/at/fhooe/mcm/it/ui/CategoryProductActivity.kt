package at.fhooe.mcm.it.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.adapter.ProductAdapter
import at.fhooe.mcm.it.service.HardwarePurchaseService
import kotlinx.android.synthetic.main.activity_category_product.*
import kotlinx.coroutines.runBlocking
import java.util.stream.Collectors

class CategoryProductActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_CATEGORY = "extraCategory"
    }

    private lateinit var adapter: ProductAdapter
    private var category: String = ""

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_product)

        if (intent != null && intent.hasExtra(EXTRA_CATEGORY))
            category = intent.getStringExtra(EXTRA_CATEGORY)

        adapter = ProductAdapter(this)
        recyclerCategoryProducts.layoutManager = LinearLayoutManager(this)
        recyclerCategoryProducts.adapter = adapter
        recyclerCategoryProducts.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (category.isNotEmpty())
            supportActionBar?.title = category


        loadCategoryProducts()
    }

    private fun loadCategoryProducts() {
        runBlocking {
            val products = HardwarePurchaseService.api.getProducts()
            val categoryProducts = products.products
                    .stream()
                    .filter { it.category == category }
                    .collect(Collectors.toList())

            adapter.updateProducts(categoryProducts)
        }
    }
}
