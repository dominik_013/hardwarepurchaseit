package at.fhooe.mcm.it.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.extensions.inflate
import at.fhooe.mcm.it.ui.CategoryProductActivity

class CategoryAdapter(private val context: Context) : RecyclerView.Adapter<CategoryViewHolder>(), CategoryClickListener {

    private val categoryList: MutableList<String>

    init {
        categoryList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CategoryViewHolder {
        val inflatedView = parent.inflate(R.layout.item_category, false)
        return CategoryViewHolder(inflatedView, this)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val itemCategory = categoryList[position]
        holder.bindCategory(itemCategory)
    }

    override fun onClickCategory(position: Int) {
        val intent = Intent(context, CategoryProductActivity::class.java)
        intent.putExtra(CategoryProductActivity.EXTRA_CATEGORY, categoryList[position])
        context.startActivity(intent)
    }

    fun updateCategories(categoryList: List<String>) {
        this.categoryList.clear()
        this.categoryList.addAll(categoryList)
        notifyDataSetChanged()
    }
}