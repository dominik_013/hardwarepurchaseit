package at.fhooe.mcm.it.model

data class Settings(val stripePublishableKey: String,
                    val productCategories: List<String>)