package at.fhooe.mcm.it.model

data class ProductList(val products: List<Product>)