package at.fhooe.mcm.it.basket

import at.fhooe.mcm.it.model.Product

object BasketManager {

    private var listener: IPriceListener? = null

    private val products = mutableListOf<Product>()

    fun addProduct(product: Product) {
        products.add(product)
    }

    fun getAllProducts() = products

    fun getProductsCount() = products.size

    fun getTotalPrice() = products.map { it.price }.sum()

    fun deleteProductAtPosition(position: Int) {
        products.removeAt(position)
        listener?.onPriceUpdate(getTotalPrice())
    }

    fun deleteAllProducts() {
        products.clear()
    }

    fun getProductAtPosition(position: Int) = products[position]

    fun addPriceListener(priceListener: IPriceListener) {
        listener = priceListener
    }
}

interface IPriceListener {
    fun onPriceUpdate(newPrice: Double)
}