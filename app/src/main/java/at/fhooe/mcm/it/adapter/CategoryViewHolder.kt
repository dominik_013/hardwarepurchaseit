package at.fhooe.mcm.it.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryViewHolder(private val view: View, private val listener: CategoryClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        listener.onClickCategory(adapterPosition)
    }

    fun bindCategory(category: String) {
        view.categoryName.text = category
    }

}