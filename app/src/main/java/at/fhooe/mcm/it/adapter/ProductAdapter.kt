package at.fhooe.mcm.it.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.extensions.inflate
import at.fhooe.mcm.it.model.Product
import at.fhooe.mcm.it.ui.ProductDetailActivity

class ProductAdapter(private val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<ProductViewHolder>(), ProductClickListener {

    private val productList: MutableList<Product>

    init {
        productList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ProductViewHolder {
        val inflatedView = parent.inflate(R.layout.item_product, false)
        return ProductViewHolder(inflatedView, this)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val itemProduct = productList[position]
        holder.bindProduct(itemProduct)
    }

    override fun onClickProduct(position: Int) {
        val product = productList[position]
        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra(ProductDetailActivity.EXTRA_PRODUCT, product)
        context.startActivity(intent)
    }

    fun updateProducts(productList: List<Product>) {
        this.productList.clear()
        this.productList.addAll(productList)
        notifyDataSetChanged()
    }
}