package at.fhooe.mcm.it.adapter

interface ProductClickListener {
    fun onClickProduct(position: Int)
}