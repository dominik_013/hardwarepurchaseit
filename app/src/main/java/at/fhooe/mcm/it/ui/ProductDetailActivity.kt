package at.fhooe.mcm.it.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.basket.BasketManager
import at.fhooe.mcm.it.model.Product
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*


class ProductDetailActivity : AppCompatActivity() {

    lateinit var product: Product

    companion object {
        const val EXTRA_PRODUCT = "extraProduct"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        product = intent.getSerializableExtra(EXTRA_PRODUCT) as Product

        setSupportActionBar(productDetailToolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarLayout.title = product.title

        productDetailDescription.text = product.description
        productDetailPrice.text = String.format(resources.getString(R.string.product_detail_price), product.price)
        Picasso.get().load(product.imageUrl).into(productDetailImage)

        productDetailBuyButton.setOnClickListener { v ->
            BasketManager.addProduct(product)
            Snackbar.make(v, String.format(resources.getString(R.string.product_detail_product_added_to_cart, product.title)), Snackbar.LENGTH_LONG).show()
        }
    }
}
