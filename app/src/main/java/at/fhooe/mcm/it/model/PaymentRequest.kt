package at.fhooe.mcm.it.model

import com.google.gson.annotations.SerializedName

data class PaymentRequest(@SerializedName("fbId")
                          val facebookId: String,
                          val stripeToken: String)