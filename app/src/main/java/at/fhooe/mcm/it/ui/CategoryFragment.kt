package at.fhooe.mcm.it.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.adapter.CategoryAdapter
import at.fhooe.mcm.it.service.HardwarePurchaseService
import kotlinx.android.synthetic.main.fragment_category.view.*
import kotlinx.coroutines.runBlocking

class CategoryFragment : androidx.fragment.app.Fragment() {

    private lateinit var adapter: CategoryAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_category, container, false)

        adapter = CategoryAdapter(context!!)
        view.recyclerCategories.layoutManager = LinearLayoutManager(activity)
        view.recyclerCategories.adapter = adapter
        view.recyclerCategories.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))

        loadCategories()

        return view
    }

    private fun loadCategories() {
        runBlocking {
            val settings = HardwarePurchaseService.api.getSettings()
            adapter.updateCategories(settings.productCategories)
        }
    }
}