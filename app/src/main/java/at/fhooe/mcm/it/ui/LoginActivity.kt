package at.fhooe.mcm.it.ui

import android.app.Activity
import android.os.Bundle
import at.fhooe.hardwarepurchase.R
import com.facebook.CallbackManager
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.FacebookCallback
import com.facebook.login.widget.LoginButton
import android.content.Intent
import com.google.android.material.snackbar.Snackbar
import com.facebook.AccessToken


class LoginActivity : Activity() {

    private lateinit var callbackManager: CallbackManager
    private lateinit var loginButton: LoginButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        callbackManager = CallbackManager.Factory.create()

        loginButton = findViewById(R.id.facebook_login_button)
        loginButton.setReadPermissions("email")

        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                startMainActivity()
            }

            override fun onCancel() {
                Snackbar.make(findViewById(R.id.layout_payment), R.string.main_facebook_login_cancel, Snackbar.LENGTH_SHORT).show()
            }

            override fun onError(exception: FacebookException) {
                Snackbar.make(findViewById(R.id.layout_payment), R.string.main_facebook_login_error, Snackbar.LENGTH_SHORT).show()
            }
        })

        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired

        if (isLoggedIn) {
            startMainActivity()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun startMainActivity() {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        intent.putExtra(MainActivity.PARAM_NAVIGATION_ID, R.id.navigation_category)
        startActivity(intent)
        finish()
    }
}
