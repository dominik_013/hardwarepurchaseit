package at.fhooe.mcm.it.model

import com.google.gson.annotations.SerializedName

data class PaymentResponse(@SerializedName("payment_success")
                       val paymentSuccess: Boolean,
                           val productId: String,
                           val paymentDate: String,
                           val stripeChargeId: String)