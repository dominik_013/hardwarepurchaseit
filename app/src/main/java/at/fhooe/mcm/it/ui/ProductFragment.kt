package at.fhooe.mcm.it.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.adapter.ProductAdapter
import at.fhooe.mcm.it.service.HardwarePurchaseService
import kotlinx.android.synthetic.main.fragment_product.view.*
import kotlinx.coroutines.runBlocking

class ProductFragment : androidx.fragment.app.Fragment() {

    private lateinit var adapter: ProductAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_product, container, false)

        adapter = ProductAdapter(context!!)
        view.recyclerProducts.layoutManager = LinearLayoutManager(activity)
        view.recyclerProducts.adapter = adapter
        view.recyclerProducts.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))
        loadProducts()

        return view
    }

    private fun loadProducts() {
        runBlocking {
            val products = HardwarePurchaseService.api.getProducts()
            adapter.updateProducts(products.products)
        }
    }
}
