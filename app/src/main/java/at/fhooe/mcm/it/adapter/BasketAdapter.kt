package at.fhooe.mcm.it.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.basket.BasketManager
import at.fhooe.mcm.it.extensions.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_basket.view.*

class BasketAdapter(private val onDeleteListener: (Unit) -> Unit) : RecyclerView.Adapter<BasketViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BasketViewHolder {
        val inflatedView = parent.inflate(R.layout.item_basket, false)
        return BasketViewHolder(inflatedView, onDeleteListener)
    }

    override fun getItemCount() = BasketManager.getProductsCount()

    override fun onBindViewHolder(holder: BasketViewHolder, position: Int) {
        holder.bindProduct(position)
    }

    fun updateBasket() {
        notifyDataSetChanged()
    }
}

class BasketViewHolder(private val view: View, private val onDeleteListener: (Unit) -> Unit) : RecyclerView.ViewHolder(view) {

    fun bindProduct(position: Int) {
        val product = BasketManager.getProductAtPosition(position)

        Picasso.get().load(product.imageUrl).into(view.productImage)
        view.productTitle.text = product.title
        view.product_price.text = product.price.toString()
        view.delete_product.setOnClickListener {
            BasketManager.deleteProductAtPosition(adapterPosition)
            onDeleteListener.invoke(Unit)
        }
    }

}