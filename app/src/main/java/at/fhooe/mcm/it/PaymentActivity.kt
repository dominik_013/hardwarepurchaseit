package at.fhooe.mcm.it

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.view.View
import at.fhooe.hardwarepurchase.R
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Token
import java.lang.Exception
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import at.fhooe.mcm.it.basket.BasketManager
import at.fhooe.mcm.it.model.PaymentRequest
import at.fhooe.mcm.it.service.HardwarePurchaseService
import at.fhooe.mcm.it.ui.LoginActivity
import com.facebook.Profile
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.coroutines.runBlocking


class PaymentActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PRODUCT_IDS = "extraProductIds"
    }

    private var stripe: Stripe? = null
    private var productsToPurchase: ArrayList<String>? = null
    private var stripePublishableKey: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        progressBar.visibility = View.INVISIBLE
        btnPay.isEnabled = false

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        btnPay.setOnClickListener { view -> onClickPay(view) }

        if (intent != null && intent.hasExtra(EXTRA_PRODUCT_IDS))
            productsToPurchase = intent.getStringArrayListExtra(EXTRA_PRODUCT_IDS)

        runBlocking {
            val settings = HardwarePurchaseService.api.getSettings()
            stripePublishableKey = settings.stripePublishableKey
            stripe = Stripe(this@PaymentActivity, stripePublishableKey)
            btnPay.isEnabled = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onClickPay(view: View) {
        btnPay.isEnabled = false
        progressBar.visibility = View.VISIBLE
        val imm: InputMethodManager = this@PaymentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        val card = cardInputWidget.card
        if (card == null) {
            Snackbar.make(findViewById(R.id.layout_payment), R.string.payment_error_invalid_card_data, Snackbar.LENGTH_SHORT).show()
            return
        }

        if (productsToPurchase.isNullOrEmpty()) {
            Snackbar.make(findViewById(R.id.layout_payment), R.string.payment_error_no_products_selected, Snackbar.LENGTH_SHORT).show()
            return
        }

        if (stripe == null) {
            return
        }

        val products = productsToPurchase?.toList()
        if (products != null) {
            var numProducts = products.size
            for (product in products) {
                stripe?.createToken(card, object : TokenCallback {

                    override fun onSuccess(token: Token) {
                        val paymentRequest = PaymentRequest(Profile.getCurrentProfile().id, token.id)
                        runBlocking {
                            val response = HardwarePurchaseService.api.buyProduct(product, paymentRequest)
                            Log.i("Purchase Success", response.toString())
                            numProducts--

                            if (numProducts == 0) {
                                BasketManager.deleteAllProducts()
                                Toast.makeText(this@PaymentActivity, "Purchase success", Toast.LENGTH_LONG).show()
                                btnPay.isEnabled = true
                                progressBar.visibility = View.INVISIBLE

                                finish()
                            }
                        }
                    }

                    override fun onError(error: Exception) {
                        btnPay.isEnabled = true
                        progressBar.visibility = View.INVISIBLE
                        Snackbar.make(findViewById(R.id.layout_payment), R.string.payment_error_creating_token, Snackbar.LENGTH_SHORT).show()
                    }
                })
            }
        }
    }
}
