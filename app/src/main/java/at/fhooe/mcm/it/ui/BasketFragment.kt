package at.fhooe.mcm.it.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import at.fhooe.hardwarepurchase.R
import at.fhooe.mcm.it.PaymentActivity
import at.fhooe.mcm.it.adapter.BasketAdapter
import at.fhooe.mcm.it.basket.BasketManager
import at.fhooe.mcm.it.basket.IPriceListener
import kotlinx.android.synthetic.main.fragment_basket.*

class BasketFragment : androidx.fragment.app.Fragment(), IPriceListener {

    private lateinit var adapter: BasketAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_basket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateVisibility()

        basket_list.itemAnimator = DefaultItemAnimator()
        adapter = BasketAdapter {
            basket_list.adapter?.notifyDataSetChanged()
            updateVisibility()
        }
        basket_list.adapter = adapter
        payment_button.setOnClickListener {
            val intent = Intent(context, PaymentActivity::class.java)
            val products = BasketManager.getAllProducts()
            val productIds = ArrayList<String>()
            for (product in products) {
                productIds.add(product.id)
            }
            intent.putExtra(PaymentActivity.EXTRA_PRODUCT_IDS, productIds)
            startActivity(intent)
        }
        total_price.text = String.format(resources.getString(R.string.basket_total_price), BasketManager.getTotalPrice())
        BasketManager.addPriceListener(this)
    }

    override fun onResume() {
        super.onResume()
        adapter.updateBasket()
        updateVisibility()
    }

    override fun onPriceUpdate(newPrice: Double) {
        total_price.text = String.format(resources.getString(R.string.basket_total_price), newPrice)
    }

    private fun updateVisibility() {
        empty_text.visibility = if (BasketManager.getProductsCount() == 0) View.VISIBLE else View.GONE
        content.visibility = if (BasketManager.getProductsCount() == 0) View.GONE else View.VISIBLE
    }
}
