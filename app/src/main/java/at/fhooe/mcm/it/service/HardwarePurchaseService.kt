package at.fhooe.mcm.it.service

import at.fhooe.mcm.it.model.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface HardwarePurchaseService {
    companion object {
        private const val BASE_URL = "https://mcm-it.herokuapp.com"

        val api: HardwarePurchaseService by lazy {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            retrofit.create(HardwarePurchaseService::class.java)
        }
    }

    @GET("/products")
    suspend fun getProducts(): ProductList

    @GET("/products?fbId={facebookId}")
    suspend fun getProductsForUser(@Path("facebookId") facebookId: Int): ProductList

    @GET("/products/{id}")
    suspend fun getProductWithId(@Path("id") id: String): Product

    @POST("/products/{id}/buy")
    suspend fun buyProduct(@Path("id") id: String, @Body paymentRequest: PaymentRequest): Response<PaymentResponse>

    @GET("/settings")
    suspend fun getSettings(): Settings

}